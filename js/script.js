'use strict';

var $root = jQuery(':root');
var scrollSpeed = 800;
var $navbar = jQuery('#navbar');

jQuery(document).ready(function ($) {
    OnResize();

    // Btn-return //
    $('#btn-return').btnReturn({
        speed: scrollSpeed
    });

    // Banner video
    $('.s-banner-play').click(function () {
        $(this).next('.s-banner-video')[0].play();
        $(this).next('.s-banner-video').addClass('active');
    });

    $('.s-banner-video').on('play', function () {
        $(this).attr('controls', '');
        $(this).prev('.s-banner-play').hide();
    });

    // Menu //
    // on btn-menu click
    $('#btn-menu').click(function () {
        $('#nav-parent').toggleClass('open');
        $('body').toggleClass('nav-open');
    });

    // Contact form
    $(document).on('wpcf7mailsent', function () {
        window.location.href = window.location.hostname + '/thank-you';
    });

    // Popup
    $('a[href="#register"]').click(function () {
        $('.popup-contact').addClass('active');
    });
    $('.popup-contact-close').click(function () {
        $('.popup-contact').removeClass('active');
    });

    /// On resize of window ///
    $(window).resize(function () {
        // Call everything that must be done initially and on resize
        OnResize();

    });

    /// On scroll of window ///
    $(window).scroll(function () {
    });

    $(window).on("load", function () {
    });

});

// Initially and on resize
function OnResize() {
}