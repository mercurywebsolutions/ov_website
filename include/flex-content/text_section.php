<?php
$section_classes = "";
$align = get_sub_field('text_align');
$uppercase = get_sub_field('capitalised_text');
$section_classes .= $align;

if ($uppercase) { $section_classes .= ' uppercase'; }
?>
<section class="s-text <?php echo $section_classes ?>">
    <?php if ($field = get_sub_field('section_heading')) : ?>
    <h2 class="s-text-heading"><?php echo $field; ?></h2>
    <?php endif; ?>
    <?php if ($field = get_sub_field('section_text')) : ?>
    <div class="s-text-text"><?php echo $field; ?></div>
    <?php endif; ?>
</section>