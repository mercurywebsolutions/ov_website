<?php
$section_classes = "";
$img = get_sub_field('banner_image');
$vid = get_sub_field('banner_video', false);
?>
<section class="s-banner <?php echo $section_classes ?>">
    <?php if ($img && !$vid) : ?>
    <img class="s-banner-image" src="<?php echo $img ?>">
    <?php elseif ($vid) : ?>
    <?php if (!$img) { $img = ""; } ?>
    <div class="s-banner-video-wrapper">
        <button class="s-banner-play" type="button">
            <img src="<?php bloginfo('template_directory'); ?>/img/video-ring.svg">
            <p>Play Movie</p>
        </button>
        <video class="s-banner-video" controlsList="nodownload noremoteplayback" poster="<?php echo $img ?>">
            <source type="video/mp4" src="<?php echo $vid ?>" />
        </video>
    </div>
    <?php endif; ?>
    <?php if ($field = get_sub_field('banner_caption')) : ?>
    <div class="s-banner-caption"><?php echo $field; ?></div>
    <?php endif; ?>
</section>