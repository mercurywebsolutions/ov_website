<?php
function printColumn($fieldName)
{
    $col = get_sub_field($fieldName);
    if ($col) {
        $type = $col['content_type'];
        $column_classes = '';
        $column_classes .= $type;
        if ($type == 'image') {
            $column_classes .=  ' ' . $col['column_layout'];
        } elseif ($type == 'text') {
            $column_classes .=  ' style-' . $col['column_style'];
        }
        ?>
<div class="s-column-col <?php echo $column_classes; ?>">
    <?php if ($type == 'image' && $field = $col['column_image']) : ?>
    <img class="" src="<?php echo $field; ?>">
    <?php elseif ($type == 'text') :
                            if ($field = $col['column_heading']) : ?>

    <h2 class=""><?php echo $field; ?></h2>

    <?php endif; ?>
    <?php if ($field = $col['column_text']) : ?>
    <div class=""><?php echo $field; ?></div>
    <?php endif;
                    endif; ?>

    <?php if ($field = $col['column_caption']) : ?>
    <p class="s-column-caption"><?php echo $field; ?></p>
    <?php endif; ?>
</div>
<?php
    }
}

?>