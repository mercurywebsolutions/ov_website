<div class="contact-inner">
    <?php if ($field = get_field('contact_heading', 'options')) : ?>
    <h2 class="contact-heading"><?php echo $field; ?></h2>
    <?php endif; ?>
    <?php if ($field = get_field('contact_subtitle', 'options')) : ?>
    <p class="contact-subtitle"><?php echo $field; ?></p>
    <?php endif; ?>
    <?php echo do_shortcode('[contact-form-7 id="154" title="Contact form"]'); ?>
    <?php if (have_rows('contact_lines', 'options')) : ?>
    <div class="contact-section">
        <p class="contact-section-label">Contact</p>
        <div class="contact-lines">
            <?php while (have_rows('contact_lines', 'options')) : the_row(); ?>
            <div class="contact-line">
                <?php if ($field = get_sub_field('name')) : ?>
                <p><?php echo $field; ?></p>
                <?php endif; ?>
                <?php if ($field = get_sub_field('phone_number')) : ?>
                <a href="tel:<?php echo $field; ?>"><?php echo $field; ?></a>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
    <?php if (have_rows('contributors', 'options')) : ?>
    <div class="contact-section">
        <p class="contact-section-label">A project by</p>
        <div class="contributors">
            <?php while (have_rows('contributors', 'options')) : the_row(); ?>
            <div class="contributor">
                <?php if ($field = get_sub_field('logo')) : ?>
                <img src="<?php echo $field; ?>">
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
</div>