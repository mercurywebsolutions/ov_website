    <?php if (!is_front_page() && !is_page('thank-you')) : ?>
    <footer class="section-contact">
        <?php include __DIR__ . '/include/contact.php'; ?>
    </footer>
    <?php endif; ?>
    <?php wp_footer(); ?>
    </body>
</html>