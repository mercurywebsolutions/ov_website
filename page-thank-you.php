<?php get_header(); ?>
<div class="thank-you">
    <p>Thank you for registering your interest.</p>
    <p>A representative will contact you shortly.</p>
</div>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '878294318939083');
fbq('track', 'PageView');
fbq('track', 'REA_Event', {
advertiser: 'CHHProperty_Overture_196778',
tracking_event: 'ThankYouPage_CHHProperty_Overture',
});
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=878294318939083&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<?php get_footer(); ?>