<?php
$colContentLeft = get_sub_field('column_fields');
$colContentRight = get_sub_field('2_column_fields');

$colLeftType = $colContentLeft['content_type'];
$colRightType = $colContentRight['content_type'];

$section_classes = "";
$section_classes .= "col-1-" . $colLeftType . " col-2-" . $colRightType;


?>
<section class="s-column <?php echo $section_classes ?>">
    <?php
    printColumn('column_fields');
    printColumn('2_column_fields');
    ?>

</section>