<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" type="image/png" href="img/favicon.png">
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php $video = get_field('navbar_video', 'options', false); ?>
    <?php echo do_shortcode('[wp-video-popup mute="1" video="' . $video . '"]'); ?>
    <?php
    $image = get_field('background_image', 'options');
    if (!$image) {
        $image = '';
    }
    ?>
    <div class="popup-contact" style="background-image: url(<?php echo $image; ?>);">
        <button type="button" class="popup-contact-close">
            <img src="<?php bloginfo('template_directory'); ?>/img/close.png">
        </button>
        <?php include __DIR__ . '/include/contact.php'; ?>
    </div>
    <div id="navbar">
        <div id="logo-mobile">
            <p id="logo-head">Spring Release</p>
            <p id="logo-sub-head">By Overture</p>
        </div>
        <button id="btn-menu" type="button">
            <i class="material-icons hamburger">menu</i>
            <i class="material-icons exit">close</i>
        </button>
        <div id="nav-parent">
            <div id="nav-parent-inner">
                <?php wp_nav_menu(array('theme_location' => 'menu_left', 'menu_id' => 'nav-list-left', 'menu_class' => 'menu container', 'container' => 'nav', 'container_class' => 'nav',)); ?>
                <div id="logo">
                    <p id="logo-head">Spring Release</p>
                    <p id="logo-sub-head">By Overture</p>
                </div>
                <?php wp_nav_menu(array('theme_location' => 'menu_right', 'menu_id' => 'nav-list-right', 'menu_class' => 'menu container', 'container' => 'nav', 'container_class' => 'nav',)); ?>
            </div>
        </div>
    </div>
    <?php include __DIR__ . '/include/print_column.php'; ?>