<?php

/************************************************************************
 * function.php:
 * 		- Include all relevant styles & scripts
 * Note: Index files must contain get_header() to enque styles/scripts
 ***********************************************************************/
function load_sources()
{
    wp_enqueue_style('matIcons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style('initial', get_template_directory_uri() . '/css/initial.css');
    wp_enqueue_style('btn-return', get_template_directory_uri() . '/css/widgets/btn-return.css');
    wp_enqueue_style('navbar', get_template_directory_uri() . '/css/theme/navbar.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/css/theme/style.css');
    wp_enqueue_style('z-indexes', get_template_directory_uri() . '/css/theme/z-indexes.css');
    wp_enqueue_style('fonts', get_template_directory_uri() . '/css/theme/fonts.css');
    wp_enqueue_style('home', get_template_directory_uri() . '/css/theme/home.css');
    wp_enqueue_style('column-section', get_template_directory_uri() . '/css/theme/sections/column_section.css');
    wp_enqueue_style('text-section', get_template_directory_uri() . '/css/theme/sections/text_section.css');
    wp_enqueue_style('banner-section', get_template_directory_uri() . '/css/theme/sections/banner_section.css');
    wp_enqueue_style('contact', get_template_directory_uri() . '/css/theme/sections/contact.css');
    wp_enqueue_style('thank-you', get_template_directory_uri() . '/css/theme/thank-you.css');
    wp_enqueue_style('404', get_template_directory_uri() . '/css/404.css');

    /* WP's default jquery library seems to be out of date? 
    You cannot manipulate css varlables via jquery without 3.2.2 or heigher */
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', array(), null, true);
    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array(), null, true);
    wp_enqueue_script('fade-at', get_template_directory_uri() . '/js/widgets/fade_at.js', array('jquery-ui-core', 'jquery-ui-widget'), null, true);
    wp_enqueue_script('btn-return', get_template_directory_uri() . '/js/widgets/btn_return.js', array('jquery-ui-core', 'jquery-ui-widget'), null, true);
    wp_enqueue_script('scroll-to', get_template_directory_uri() . '/js/widgets/scroll_to.js', array('jquery-ui-core', 'jquery-ui-widget'), null, true);
    wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', array('jquery', 'scroll-to'), null, true);
}
add_action('wp_enqueue_scripts', 'load_sources');


add_action('after_setup_theme', 'register_custom_nav_menus');
function register_custom_nav_menus()
{
    register_nav_menus(array(
        //'main_menu' => 'Main Menu',
        'menu_left' => 'Menu Left',
        'menu_right' => 'Menu Right',
    ));
}

add_theme_support('post-thumbnails');

if (function_exists('acf_add_options_page')) {	
	acf_add_options_page();	
}