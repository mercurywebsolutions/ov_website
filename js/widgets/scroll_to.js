; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.scrollTo", {
        options: {
            destination: 0,
            navbar: null,
            speed: 600,
            easing: "swing"
        },

        _create: function () {
            this.marginTop = 0;
            this.navbarHeight = 0;

            var marginTop = parseInt(this.options.destination.css('margin-top'));
            if (marginTop >= 0) {
                this.marginTop = marginTop;
            }

            if (this.options.navbar != null) {
                this.navbarHeight = this.options.navbar.outerHeight();
            }

            // On 'element' event
            this._on(this.element, {
                // Click
                click: function () {
                    this._scroll();
                }
            });
        },
        _scroll: function () {
            var destination = Math.ceil(this.options.destination.offset().top - this.marginTop - this.navbarHeight);

            // If animation is not already happening AND window is not already at destination
            if ($('html, body').is(':animated') == false && $(window).scrollTop() != destination) {
                // Scroll to destination
                $('html, body').animate({
                    scrollTop: destination
                }, this.options.speed, this.options.easing);
            }
        }
    });

})(jQuery, window, document);